import React from 'react'

class App extends React.Component {

    constructor() {
        super()
        this.state = {
            tasks: [],
            newTask: ''
        }
    }

    handleInput(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    addTask() {
        let tempTasks = this.state.tasks
        tempTasks.push(this.state.newTask)

        this.setState({
            tasks: tempTasks,
            newTask: ''
        }, (newState) => {
            //
        })
    }

    render() {
        return (
            <>
                <h1>Todo List</h1> 

                <input type="text" name="newTask" value={this.state.newTask} 
                    onChange={event => this.handleInput(event)} />
                <input type="button" value="+" onClick={() => this.addTask() }/>

                <ul>
                    {
                        this.state.tasks.map( (task, index) =>
                            <li key={index}>{task}</li>
                        )
                    }                    
                </ul>
            </>
        )
    }

}

export default App