import { useEffect, useState } from "react"

const AppHook = () => {
    const [tasks, setTasks] = useState([])
    const [newTask, setNewTask] = useState('')

    const addTask = () => {
        setTasks([...tasks, newTask])
        setNewTask('')
    }

    useEffect(() => {
        let tempTasks = localStorage.getItem('tasks')
        if (tempTasks !== null) {
            setTasks(JSON.parse(tempTasks))
        }
    }, [])

    useEffect(() => {
        localStorage.setItem('tasks', JSON.stringify(tasks))
    }, [tasks])

    useEffect( () => {
        console.log('Componente atualizado')
    })

    return (
        <>
            <h1>Todo List - com Hooks</h1> 

            <input type="text" name="newTask" value={newTask} 
                onChange={event => setNewTask(event.target.value)} />
            <input type="button" value="+" onClick={() => addTask()} />

            <ul>
                {
                    tasks.map( (task, index) =>
                        <li key={index}>{task}</li>
                    )
                }                    
            </ul>
        </>
    )
}

export default AppHook